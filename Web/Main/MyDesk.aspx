﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="MyDesk.aspx.cs" Inherits="MyDesk" %>
<html>
	<head>
	<title><%=System.Configuration.ConfigurationManager.AppSettings["SYSTitle"]%></title>
     <LINK href="../Style/Style.css" type="text/css" rel="STYLESHEET">
     <script type="text/javascript" src="../js/jquery.min.js"></script>
</head>
<body>
<script type="text/javascript">
function SwitchMenu(theClass){
 var alldivTags=document.getElementsByTagName("div");
 for(i=0;i<alldivTags.length;i++){
  if(alldivTags[i].className==theClass) {   
   if(alldivTags[i].style.display=='none'){
    alldivTags[i].style.display='block';
   }else{
    alldivTags[i].style.display='none';
   }
  }
 }
}
</script>

<script language="javascript" >
function _delmodel(a)
{
    msg="确认不显示此模块吗?";
    if(window.confirm(msg))
    {    
      window.location.href ='MyDeskDel.aspx?ModelName='+a;
    }
}
</script>

    <form id="form1" runat="server">
    <div>    
     <table id="PrintHide" style="width: 100%;" border="0" cellpadding="0" cellspacing="0">            
            <tr>
                <td valign="middle" style="border-bottom: #006633 1px dashed; height: 30px; width: 140px; ">&nbsp;<img src="../images/BanKuaiJianTou.gif" />
                我的桌面&nbsp;</td>
                <td style="border-bottom: #006633 1px dashed; height: 30px" valign="middle">
                    </td>
                <td align="right" valign="middle" style="border-bottom: #006633 1px dashed; height: 30px;">
                    <a href="MyDeskSetting.aspx"> <img align="absMiddle" border="0" src="../images/Button/Setting.gif" /></a>
                    </td>
            </tr>
            <tr style=" height:10px;"><td></td></tr>
        </table>
    </div>
        <asp:Panel ID="Panel1"  runat="server" Width="100%">
        <asp:Label ID="Label1" runat="server"></asp:Label>   
        </asp:Panel>    
    </form>
    <div style=" clear:both"></div>
    <style type="text/css">
    .kuaijie{width:100%;height:80px; margin-top:20px;}
    .kuaijie ul{ margin:0px; padding:0px;}
    .kuaijie li{ width:19%; height:100px; float:left;}
    .kuaijie li a{ display:block; width:90%;text-align:center; color:#fff; height:80px; line-height:80px; font-size:20px;}
    .kuaijie li a:hover{ background-color:#204769;}
    </style>
    <div class="kuaijie">
    <ul>
    <li style=" margin-left:5px;"><a style=" background-color:#a79c47; font-size:20px; color:#fff;" href="/web/NWorkFlow/NWorkToDoAdd.aspx?FormID=27&WorkFlowID=28">发文处理签</a></li>
    <li><a style=" background-color:#2e8bdf;font-size:20px; color:#fff;" href="/Web/NWorkFlow/NWorkToDoAdd.aspx?FormID=38&WorkFlowID=29">收文处理签</a></li>
    <li><a style=" background-color:#24aa09;font-size:20px; color:#fff;" href="/Web/NWorkFlow/NWorkToDoAdd.aspx?FormID=22&WorkFlowID=25">盖章处理签</a></li>
    <li><a style=" background-color:#e94967;font-size:20px; color:#fff;" href="/Web/NWorkFlow/NWorkToDoAdd.aspx?FormID=36&WorkFlowID=26">信息发布处理签</a></li>
    <li><a style=" background-color:#a82023;font-size:20px; color:#fff;" href="/Web/NWorkFlow/NWorkToDoAdd.aspx?FormID=39&WorkFlowID=30">学校介绍信</a></li>
    <li><a style=" background-color:#f99c00;font-size:20px; color:#fff;" href="/Web/News/NewsList.aspx">内部新闻</a></li>
    </ul>
    </div>
</body>
</html>