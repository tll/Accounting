﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using ZWL.BLL;
using ZWL.DBUtility;

public partial class News_NewsView : System.Web.UI.Page
{
    public string sTitle="";
    public string sDate = "";
    public string sAuthor = "";
    public string sContent = "";
    public string sPic="";
    public string sFrom="";
    public string sLi = "";
    protected void Page_Load(object sender, EventArgs e)
    {
        string s=Request.QueryString["ID"].ToString();
        ReadNews(s);
    }

    public void ReadNews(string id)
    {
        string s = "select * from ERPNews where id=" + id;
        SqlDataReader dr =DbHelperSQL.GetDataReader(s);
        
        if (dr.Read()) {
            sTitle = dr["Title"].ToString();
            sDate=dr["sDate"].ToString ();
            sFrom=dr["sFrom"].ToString();
            sContent = dr["scontent"].ToString();
            sAuthor=dr["author"].ToString();
        }
        dr.Close();

        string sn = "";
        ZWL.BLL.ERPNews news= new ZWL.BLL.ERPNews();
        DataTable dt = news.GetList(10,"","ID Desc").Tables[0];
        for(int i=0 ;i<dt.Rows.Count ;i++)
        {
            sn += "<li>" +(i+1)+". <a href='NewsView.aspx?ID=" + dt.Rows [i]["ID"] +"' ";
            sn += "title='" + dt.Rows[i]["title"] + "'>" + dt.Rows[i]["title"];
            sn += "</a></li>";
        }
        sLi = sn;

        string pic = "";
        DataTable dt_pic = news.GetList(5, "sType='图文新闻'", "ID Desc").Tables[0];
        for (int j = 0; j < dt_pic.Rows.Count; j++)
        {
            pic += "<a href='NewsView.aspx?ID=" + dt_pic.Rows[j]["ID"] + "'><img src='";
            pic+=dt_pic.Rows[j]["sPic"]+"' alt='"+ dt_pic.Rows[j]["title"]+"'></a>";
        }
        sPic = pic;
    }

}