﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ZWL.BLL;

public partial class News_NewsAdd : System.Web.UI.Page
{
    string txtContent = "";
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Request.Form["TxtContent"] != null)
        {
            txtContent = Request.Form["TxtContent"].ToString();
        }

    }
    protected void ImageButton1_Click(object sender, ImageClickEventArgs e)
    {
        //保存
        ZWL.BLL.ERPNews news = new ZWL.BLL.ERPNews();
        news.sDate = DateTime.Now;
        news.author = txtAuthor.Text;
        news.sType ="文字新闻";
        news.sRoles = sRoles.SelectedValue;
        news.sFrom = txtFrom.Text;
        news.Title = txtTitle.Text;
        news.sContent = txtContent;
        news.sPic = FileUpload1.FileName;

        if (news.sType == "图文新闻" && news.sPic == "")
        {
            Response.Write("<script language='javascript'>alert('图文新闻，请选择封面图片！')</script>");
            return;
        }

        if (news.sPic != "")
        {
            string ex = System.IO.Path.GetExtension(FileUpload1.PostedFile.FileName);
            string s = DateTime.Now.Ticks.ToString() + ex;

            if (!".jpg.gif.png.bmp".Contains(ex))
            {
                Response.Write("<script language='javascript'>alert('上传的必须是图片文件！')</script>");
                return;
            }

            FileUpload1.SaveAs(System.Web.HttpContext.Current.Request.MapPath("../UploadFile/news/") + s);
            news.sPic = "../UploadFile/news/" + s;
        }

        if (news.Add(news) != 0)
        {
            ZWL.Common.MessageBox.ShowAndRedirect(this, "新闻添加成功！", "NewsAdd.aspx");
        }
        else
        { Response.Write("<script language='javascript'>alert('保存出错！')</script>"); };
    }
    protected void ImageButton2_Click(object sender, ImageClickEventArgs e)
    {
        Response.Redirect("NewsList.aspx");
    }
}