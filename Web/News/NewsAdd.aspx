﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="NewsAdd.aspx.cs" Inherits="News_NewsAdd" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>新闻管理</title>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<link rel="stylesheet" type="text/css" href="../style/style.css" charset="utf-8"/>
    <script src='../js/jquery.min.js'></script>
	<link href="../ueditor_mini/themes/default/css/umeditor.min.css" type="text/css" rel="STYLESHEET">  
	<script type="text/javascript" src="../ueditor_mini/umeditor.min.js"></script>
	<script type="text/javascript" src="../ueditor_mini/umeditor.config.js"></script>

	<style type="text/css">
	    .txt {width:357px;border:1px solid #ccc;background:#fff;height:24px;}
	    .txtInput{ margin-right:5px; padding:0 3px 0 3px; height:22px; line-height:22px; background:#FAFAFA; border:1px solid #D7D7D7; vertical-align:middle; font-size:12px; font-family:'微软雅黑'; }
	    input.normal{ width:350px; }
	    table{border-collapse:collapse;border-spacing:0;}
	    .form_table{ width:100%;border:1px solid #EDECFF; font-family:Verdana, Geneva, sans-serif; }
	    .form_table th{padding:5px 8px 5px 0;color:#333;font-weight:700;text-align:right;background:#f9f9f9;}
	    .form_table td{padding:6px 0 5px 10px;text-align:left;color:#717171;line-height:200%}
	    .form_table label{ margin-left:10px; padding:7px 0 0; font-family:"宋体"; }
	    .form_table label.attr{color:#1d1d1d}
	    .form_table label input{ margin-right:5px; vertical-align:middle;}
	    .form_table span label{ margin:0; padding:0; }
	    .form_table textarea{font-size:12px;padding:3px;color:#000;border:1px #d2d2d2 solid;vertical-align:middle; font-family:"微软雅黑";}
	    .form_table textarea.small{ width:350px; height:75px;}
	    .form_table textarea.big{ width:500px; height:350px;}
	    .form_table img.operator{ width:12px; height:12px; margin:0 6px; cursor:pointer; vertical-align:bottom; }
	</style>

	<script language="javascript">
	<!--
		$(function () {
			UM.getEditor('editor',{toolbar:[
            'source | undo redo | bold italic underline strikethrough | superscript subscript | forecolor backcolor | removeformat |',
            'insertorderedlist insertunorderedlist | selectall cleardoc | fontfamily fontsize' ,
            '| justifyleft justifycenter justifyright justifyjustify  | image']});
		});

	//-->
	</script>
</head>
<body>
<form id="form1" runat="server">
	<table id="PrintHide" style="width: 100%" border="0" cellpadding="0" cellspacing="0">
        <tr>
            <td valign="middle" style="border-bottom: #006633 1px dashed; height: 30px;">
                &nbsp;<img src="../images/BanKuaiJianTou.gif" />
                <a class="hei" href="../Main/MyDesk.aspx">桌面</a>&nbsp;>>&nbsp;新闻动态&nbsp;>>&nbsp;添加新闻
            </td>
            <td align="right" valign="middle" style="border-bottom: #006633 1px dashed; height: 30px;">
                <asp:ImageButton ID="ImageButton1" runat="server" ImageUrl="~/images/Button/Submit.jpg" OnClick="ImageButton1_Click" />
                <img src="../images/Button/JianGe.jpg" />&nbsp;
                <asp:ImageButton ID="ImageButton2" runat="server" 
                    ImageUrl="~/images/Button/BtnExit.jpg" onclick="ImageButton2_Click" />
            </td>
        </tr>
        <tr>
            <td height="3px" colspan="2" style="background-color: #ffffff"></td>
        </tr>
    </table>
	<div style="margin-top:5px;">
		<table class="form_table">
            <col width="150px"><col>
            <tbody>
            <%--<tr>
                <th>所属类别：</th>
                <td><asp:DropDownList id="sType" CssClass="select2 required" runat="server">
                    <asp:ListItem>文字新闻</asp:ListItem>
                    <asp:ListItem>图文新闻</asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>--%>
            <tr>
                
                <th>显示范围：</th>
                <td>
                <asp:RadioButtonList ID="sRoles" runat="server">
                    <asp:ListItem Selected="True" Value="1">全体教工可看</asp:ListItem>
                    <asp:ListItem Value="2">中层以上可看</asp:ListItem>
                    <asp:ListItem Value="3">高层以上可看</asp:ListItem>
                </asp:RadioButtonList>
                </td>
            </tr>
            <tr>
                <th>标题名称：</th>
                <td><asp:TextBox ID="txtTitle" runat="server" CssClass="txtInput normal required" maxlength="100" /></td>
            </tr>
            <tr>
                <th>文章作者：</th>
                <td><asp:TextBox ID="txtAuthor" runat="server" CssClass="txtInput normal" 
                        maxlength="100" ></asp:TextBox>
                </td>
            </tr>
            <tr>
                <th>文章来源：</th>
                <td><asp:TextBox ID="txtFrom" runat="server" CssClass="txtInput normal" 
                        maxlength="100" >本站</asp:TextBox>
                </td>
            </tr>
            <tr>
                <th>文章摘要：</th>
                <td><asp:TextBox ID="txtZhaiyao" runat="server" maxlength="255" TextMode="MultiLine" CssClass="small" /></td>
            </tr>
            <tr>
                <th>封面图片：</th>
                <td>
                    <asp:FileUpload ID="FileUpload1" runat="server" class="txt" /></a>
                </td>
            </tr>
            <tr>
                <th valign="top" style="padding-top:20px;">新闻内容：</th>
                <td>
					<script id="editor" name="TxtContent" type="text/plain" style="width:800px;height:280px;margin-top:2px"></script>
                </td>
            </tr>
            </tbody>
        </table>
	</div>
</form>
</body>
</html>
