﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="NewsView.aspx.cs" Inherits="News_NewsView" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html  xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title><%=sTitle %></title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<link rel="stylesheet" type="text/css" href="../style/style_js.css" charset="utf-8"/>

	<style type="text/css">
		.main {width: 1100px;margin: 0 auto;height:96%;padding-top:15px;}
		.news{min-height:300px;border:1px solid #ccc;padding:10px;text-align:left;margin-bottom:10px;padding-bottom:2px;}
		.vtitle {height:60px;border-bottom:1px dashed #ddd;text-align:center;}
		.vtitle ul{ text-align:center; display:block; width:430px; margin:0px auto;}
		.vtitle .h3{font-size:16px;font-weight:bold;;}
		.vtitle li {float:left;margin-right:20px;}
		.vcenter {margin-top:10px;margin-bottom:10px;}
		
		.news_list{float:right;width:240px;min-height:300px;height:96%;border:1px solid #ccc;padding:10px;text-align:left;}
		.news_pic {height:150px;width:238px;border:1px solid #ccc;}
		#pic {visibility:hidden;height:150px;width:238px;text-align:left;}
		#pic img{border:0;height:150px;width:238px;}

		.list_box {margin-top:10px;}
		.list_box .t1{height:24px;line-height:24px;border-bottom:1px solid #ccc;font-size:14px;font-weight:bold;padding-left:5px;}
		.list_box ul{margin-top:10px;}
		.list_box ul li {height:24px;line-height:24px;}
	</style>
</head>
<body>
<form id="form1" runat="server">
<div class="main">
	<div class="news">
		<div class="vtitle">
			<span class="h3"><%=sTitle %></span></p>
			<ul>
			 <li>日期：<%=sDate %></li>
			 <li>作者：<%=sAuthor %></li>
			 <li>来源：<%=sFrom %></li>
			</ul>
		</div>
		<div class="vcenter">
		    <%=sContent %>
		</div>
	</div>
	<div class="clear"> </div>
</div>
</form>
</body>
</html>
