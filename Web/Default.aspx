﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="_Default" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<head id="Head1" runat="server">
    <title>
        <%=System.Configuration.ConfigurationManager.AppSettings["SYSTitle"]%></title>
    <meta http-equiv="Content-Type" content="text/html;charset=UTF-8" />
    <link href="Style/login_index.css" type="text/css" rel="STYLESHEET" />
</head>
<body onload="javascript:form1.TxtUserName.focus();">
<form id="form1" runat="server">
<div class="login_top">
	<div class="logo"></div>
	<div class="font">
    <%--<span ><a href="">帮助</a></span>--%>
	<span><a href="http://www.hnact.com" title="河南省会计学校官网">网站首页</a></span>
	</div>
</div>
<div class="login_content">
	<div class="con">
		<div class="login_box">
			<table border="0" cellspacing="0" cellpadding="0">
				<tr style="height:80px">
					<td align="right" height="50" width="80">用户名：</td>
					<td align="left" width="220"><asp:TextBox ID="TxtUserName" runat="server" CssClass="text" style="padding-left:10px"></asp:TextBox></td>
				</tr>
				<tr>
					<td align="right" height="50">密码：</td>
					<td align="left"><asp:TextBox ID="TxtUserPwd"  runat="server" CssClass="text" TextMode="Password" style="padding-left:10px;width:200px; height:24px; line-height:24px; border:1px solid #46A446;"></asp:TextBox></td>
				</tr>
				<tr>
					<td height="50" colspan="2" align="center"><asp:CheckBox ID="cbRememberId" runat="server" Text="记住用户名" /></td>
				</tr>
				<tr>
					<td colspan="2" height="120" align="center">
						<asp:ImageButton ID="ImageButton1" runat="server" ImageAlign="AbsMiddle" OnClick="ImageButton1_Click" ImageUrl="~/images/img/dl.jpg"/>
					</td>
			    </tr>
			</table>
		</div>
	</div>
</div>
<div class="login_bottom" style="text-align:center">
	版权所有：河南省会计学校  <a href="http://www.hnact.com" title="">http://www.hnact.com</a>     for 2014-2015 
	ver 1.06 基础版
</div>
</form>	
</body>
</html>
