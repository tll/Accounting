﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="DoWork.aspx.cs" Inherits="NWorkFlow_DoWork" %>
<html>
	<head>
		<title><%=System.Configuration.ConfigurationManager.AppSettings["SYSTitle"]%></title>
    <LINK href="../Style/Style.css" type="text/css" rel="STYLESHEET">
  <script type="text/javascript" language="javascript" src="../JS/calendar.js"></script>
  <script type="text/javascript" src="../js/jquery.min.js"></script>
  <script src="../Controls/My97DatePicker/My97DatePicker/WdatePicker.js" type="text/javascript"></script>

  <style type="text/css">
	  *{font-size:12px;}
	.tbClass {border:1px solid silver;border-collapse:collapse;background:#fff;}
	.tbClass td{border:1px solid silver;border-collapse:collapse;padding:0 5px;height:30px;}
	h3{font-size:16px;font-weight:bold;}
	.ListOcx {width:900px;height:262px;border:1px solid #ccc;background:#fff}
	#prData {width:900px;height:282px;border:1px solid #ccc;background:#fff}
	.qm {border:1px solid #ccc;height:35px;width:630px;border-top:0px;background:#fff;padding-left:50px;line-height:35px;}

	.datagrid-cell-group {height:34px;}
	.sumok{text-align:center;}

  </style>
<script language="javascript">
	function selectUser(imgidstr)
			{            
				var wName;
				var RadNum=Math.random();            
				wName=window.showModalDialog('../Main/SelectUser.aspx?Radstr='+RadNum,'','dialogWidth:350px;DialogHeight=400px;status:no;help:no;resizable:yes;');            
				if(wName==null||wName=="")
				{}
				else
				{
					imgidstr.value=wName;                          
				}                
			}
	function selectBuMen(imgidstr)
			{            
				var wName;
				var RadNum=Math.random();            
				wName=window.showModalDialog('../Main/SelectDanWei.aspx?Radstr='+RadNum,'','dialogWidth:350px;DialogHeight=400px;status:no;help:no;resizable:yes;');            
				if(wName==null||wName=="")
				{}
				else
				{
					imgidstr.value=wName;                          
				}                
			}
	function selectyinzhang(imgidstr)
			{            
				var wName;
				var RadNum=Math.random();            
				wName=window.showModalDialog('../Main/SelectYinZhang.aspx?Radstr='+RadNum,'','dialogWidth:350px;DialogHeight=400px;status:no;help:no;resizable:yes;');            
				if(wName==null||wName=="")
				{}
				else
				{
				   // imgidstr.src="http://"+window.location.host+"<%=System.Configuration.ConfigurationManager.AppSettings["OARoot"] %>/UploadFile/"+wName;
				   imgidstr.src="../UploadFile/"+wName;                          
				}                
			}
	  function selectShouXie(imgidstr)
			{            
				var wName;
				var RadNum=Math.random();            
				wName=window.showModalDialog('../Main/InsertQianMing.aspx?Radstr='+RadNum,'','dialogWidth:350px;DialogHeight=400px;status:no;help:no;resizable:yes;');            
				if(wName==null||wName=="")
				{}
				else
				{
					imgidstr.src="http://"+window.location.host+"<%=System.Configuration.ConfigurationManager.AppSettings["OARoot"] %>/UploadFile/"+wName;                          
				}                
			}
	  
	  function PrintTable()
		{
			document.getElementById("PrintHide") .style.visibility="hidden"    
			print();
			document.getElementById("PrintHide") .style.visibility="visible"    
		}
		
		
		function _change()
	{
	   var text=form1.DropDownList1.value;
	   if (text !="请选择")
	   {
		   document.getElementById('TextBox1').value +=text+"\r\n";
	   }
	}
    function SelectUser() {
            var wName;
            var RadNum=Math.random();
            wName=window.showModalDialog('../Main/SelectUser.aspx?TableName=ERPUser&LieName=UserName&Condition='+
            document.getElementById('TextBox4').value+'&Radstr='+RadNum,'','dialogWidth:350px;DialogHeight=400px;status:no;help:no;resizable:yes;');
            if(wName==null){}else{document.getElementById('TextBox4').value=wName;};
        }
</script>
<script language="javascript">
    Date.prototype.Format = function (fmt) { //author: meizz 
        var o = {
            "M+": this.getMonth() + 1, //月份 
            "d+": this.getDate(), //日 
            "h+": this.getHours(), //小时 
            "m+": this.getMinutes(), //分 
            "s+": this.getSeconds(), //秒 
            "q+": Math.floor((this.getMonth() + 3) / 3), //季度 
            "S": this.getMilliseconds() //毫秒 
        };
        if (/(y+)/.test(fmt)) fmt = fmt.replace(RegExp.$1, (this.getFullYear() + "").substr(4 - RegExp.$1.length));
        for (var k in o)
            if (new RegExp("(" + k + ")").test(fmt)) fmt = fmt.replace(RegExp.$1, (RegExp.$1.length == 1) ? (o[k]) : (("00" + o[k]).substr(("" + o[k]).length)));
        return fmt;
    }


    $(function () {
        var fid = getQueryString("fid");
        if (fid == '29') {//收文处理签
            var xx = $("#textarea001").val();
            str = xx.replace(/<br\/>/g, "\n");
            $("#TextArea350714174").val(str);
        }
    });

    function donull(obj) {
        if ($.trim(obj.val()) != "") {
        }
        else {
            obj.val($("#TextBox1").val());
        }
    }

    function shenhe() {
        var mydate = new Date();
        var nowtime = mydate.Format("yyyy-MM-dd hh:mm:ss"); //获取当前年份
		//----判断是否是预先设定的流程--------------
        var fid = getQueryString("fid");
        var line = "<br/> ------------------------------------------------------------- <br/>";
        //发文审批流程-----------------------------------------------------
		if (fid == '28') {
		    switch ($('#Label2').text()) {
		        case "科室主管意见":
		            donull($("#Text1358028582"));
		            //$("#Text1358028582").val($("#TextBox1").val());
		            $("#Text452015055").val($("#usename001").val());
		            $("#Text1763467131").val(nowtime);
		            break;
		        case "办公室核搞":
		            donull($("#Text875886298"));
		            //$("#Text875886298").val($("#TextBox1").val());
		            $("#Text1876651637").val($("#usename001").val());
		            $("#Text722885637").val(nowtime); break;
		        case "校领导批示":
		            donull($("#Text1780770591"));
		            //$("#Text1780770591").val($("#TextBox1").val());
		            $("#Text1166758394").val($("#usename001").val());
		            $("#Text447668635").val(nowtime); break;
		        case "打印处理":
		            $("#Text948165227").val($("#usename001").val());
		            $("#Text1019936473").val(nowtime); break;
		        case "办理并归档":
		            $("#Text1214671477").val($("#usename001").val());
		            $("#Text927105675").val(nowtime); break;
		        default:
		            //alert($('#Label2').text());
		    }
		}
        //收文审批流程----------------------------------------------------------
		else if (fid == '29') {
		    switch ($('#Label2').text()) {
		        case "办公室意见":
		            donull($("#Text1358560064"));
		            //$("#Text1358560064").val($("#TextBox1").val());
		            $("#Text476218800").val($("#usename001").val());
		            $("#Text1915536153").val(nowtime);
		            break;
		        case "领导批示":
		            donull($("#Text361508859"));
		            //$("#Text361508859").val($("#TextBox1").val());
		            $("#Text1731295790").val($("#usename001").val());
		            $("#Text734123857").val(nowtime); break;
		        case "办公室办理":
		            donull($("#Text394535454"));
		            //$("#Text394535454").val($("#TextBox1").val());
		            $("#Text1791380229").val($("#usename001").val());
		            $("#Text1204228975").val(nowtime); break;
		        case "科室接收":
		            var xx1 = "【 " + $("#depar001").val() + " 】" + "-" + $("#usename001").val() + "-" + nowtime + "-" + $("#TextBox1").val() + line + $("#textarea001").val();
		            $("#textarea001").val(xx1);
		            $("#Text1019936473").val(nowtime); break;
		        case "存档办理":
		            $("#Text199527158").val($("#usename001").val());
		            $("#Text1129343100").val(nowtime); break;
		        default:
		            //alert($('#Label2').text());
		    }
		}
		//盖章审批流程----------------------------------------------------------
		else if (fid == '25') {
		    switch ($('#Label2').text()) {
		        case "科室意见":
		            donull($("#Text392850450"));
		            //$("#Text392850450").val($("#TextBox1").val());
		            $("#Text1516755306").val($("#usename001").val());
		            $("#Text110358819").val(nowtime);
		            break;
		        case "领导审批":
		            donull($("#Text955820889"));
		            //$("#Text955820889").val($("#TextBox1").val());
		            $("#Text165641813").val($("#usename001").val());
		            $("#Text1096867780").val(nowtime); break;
		        case "办理并归档":
		            $("#Text1261138054").val($("#usename001").val());
		            $("#Text62581673").val(nowtime); break;
		        default:
		            //alert($('#Label2').text());
		    }
		}
		//网站信息发布流程----------------------------------------------------------
		else if (fid == '26') {
		switch ($('#Label2').text()) {
		    case "科室意见":
		        donull($("#Text671039620"));
		            //$("#Text671039620").val($("#TextBox1").val());
		            $("#Text1199562575").val($("#usename001").val());
		            $("#Text161773813").val(nowtime);
		            break;
		        case "办公室核搞":
		            donull($("#Text978575155"));
		            //$("#Text978575155").val($("#TextBox1").val());
		            $("#Text369849384").val($("#usename001").val());
		            $("#Text1267774568").val(nowtime); break;
		        case "领导批示":
		            donull($("#Text919777909"));
		            //$("#Text919777909").val($("#TextBox1").val());
		            $("#Text522779133").val($("#usename001").val());
		            $("#Text551655310").val(nowtime);
		            break;
		        case "执行发布并归档":
		            donull($("#Text649219171"));
		            $("#Text925495910").val($("#usename001").val());
		            //$("#Text649219171").val($("#TextBox1").val());
		            $("#Text1462031185").val(nowtime); break;
		        default:
		            //alert($('#Label2').text());
		    }
		}
        else{}
		var content = document.getElementById("Label3").innerHTML;
		document.getElementById("TextBox3").value = content;
        return true;
	}

	function getQueryString(name) {
		var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)", "i");
		var r = window.location.search.substr(1).match(reg);
		if (r != null) return unescape(r[2]); return null;
	}

	function getStrCompare(SN,CN) {
		var patt = new RegExp(CN);
		var str = SN;

		if(patt.test(str)){
		  return true; 
		}else{
		  return false;; 
		}

	}
//-->
</script>
</head>
<body onload="Load_Do();">
    <form id="form1" runat="server">
    <input type="hidden" value="<%= ZWL.Common.PublicMethod.GetSessionValue("UserName")%>" id="usename001"/>
    <input type="hidden" value="<%= ZWL.Common.PublicMethod.GetSessionValue("Department")%>" id="depar001"/>
    <div>    
     <table id="PrintHide" style="width: 100%" border="0" cellpadding="0" cellspacing="0">            
            <tr>
                <td valign="middle" style="border-bottom: #006633 1px dashed; height: 30px;">&nbsp;<img src="../images/BanKuaiJianTou.gif" />
                <a class="hei" href="../Main/MyDesk.aspx">桌面</a>&nbsp;>>&nbsp;审批流程&nbsp;>>&nbsp;办理工作
                </td>
                <td align="right" valign="middle" style="border-bottom: #006633 1px dashed; height: 30px;">
                    &nbsp;<asp:ImageButton ID="ImageButton1" runat="server" ImageUrl="../images/Button/BtnPrint.jpg"
                        OnClick="ImageButton1_Click" />
                    <img src="../images/Button/JianGe.jpg" />&nbsp;
                    <img class="HerCss" onclick="javascript:window.history.go(-1)" src="../images/Button/BtnExit.jpg" />&nbsp;</td>
            </tr>
            <tr>
            <td height="3px" colspan="2" style="background-color: #ffffff"></td>
        </tr>
        </table>
    <table style="width: 100%" bgcolor="#999999" border="0" cellpadding="2" cellspacing="1">            
        
        <tr>
            <td align="right" style="width: 170px; background-color: #D6E2F3; height: 25px;" >
                工作名称：</td>
                <td style="background-color: #ffffff; height: 25px; padding-left:5px;" >
                    <asp:Label ID="Label1" runat="server"></asp:Label>
                    &nbsp; &nbsp; 当前节点：<asp:Label ID="Label2" runat="server"></asp:Label>
                    &nbsp; &nbsp; &nbsp;<asp:HyperLink ID="HyperLink1" runat="server" Font-Underline="True"
                        Target="_blank">流程图</asp:HyperLink></td>
        </tr>
        <tr class="formyuansu">
            <td colspan="2" style="border-right: #000000 1px solid; border-top: #000000 1px solid;
                padding-left: 5px; border-left: #000000 1px solid; border-bottom: #000000 1px solid;
                height: 25px; background-color: #ffffff">
                <asp:Label ID="Label3" runat="server"></asp:Label>
                <asp:TextBox ID="TextBox3" runat="server" Style="display: none"></asp:TextBox></td>
        </tr>
        <tr>
            <td align="right" style="width: 170px; height: 25px; background-color: #D6E2F3">
                附件列表：</td>
            <td style="padding-left: 5px; height: 25px; background-color: #ffffff">
                <asp:CheckBoxList ID="CheckBoxList1" runat="server" RepeatColumns="4" RepeatDirection="Horizontal">
                </asp:CheckBoxList>&nbsp;<asp:ImageButton ID="ImageButton3" runat="server" CausesValidation="False"
                    ImageAlign="AbsMiddle" ImageUrl="../images/Button/DelFile.jpg" OnClick="ImageButton3_Click" />
					 &nbsp; &nbsp;&nbsp;
        <asp:ImageButton ID="ImageButton5" runat="server" CausesValidation="False" ImageAlign="AbsMiddle"
            ImageUrl="~/images/Button/ReadFile.gif" OnClick="ImageButton4_Click" />
        &nbsp; &nbsp;&nbsp;
        <asp:ImageButton ID="ImageButton6" runat="server" CausesValidation="False" ImageAlign="AbsMiddle"
            ImageUrl="~/images/Button/EditFile.gif" OnClick="ImageButton5_Click" />
                    </td>
        </tr>
        <tr>
            <td align="right" colspan="2" style="height: 25px; background-color: #D6E2F3; text-align: center">
                <strong>我的审批</strong></td>
        </tr>
        <tr>
            <td align="right" style="width: 170px; height: 25px; background-color: #D6E2F3">
                常用审批：</td>
            <td style="padding-left: 5px; height: 25px; background-color: #ffffff">
                <asp:DropDownList ID="DropDownList1" onchange="_change()" runat="server" Width="350px">
                    <asp:ListItem>请选择</asp:ListItem>
                </asp:DropDownList></td>
        </tr>
        <tr>
            <td align="right" style="width: 170px; height: 25px; background-color: #D6E2F3">
                审批意见：</td>
            <td style="padding-left: 5px; height: 25px; background-color: #ffffff">
                <asp:TextBox ID="TextBox1" runat="server" Height="100px" TextMode="MultiLine" Width="350px"></asp:TextBox>
                <asp:TextBox ID="TextBox4" runat="server" style="height:0;width:0"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td align="right" style="width: 170px; height: 25px; background-color: #D6E2F3">
                审批附件：</td>
            <td style="padding-left: 5px; height: 25px; background-color: #ffffff">
                <asp:FileUpload ID="FileUpload1" runat="server" Width="351px" /></td>
        </tr>
        <tr>
            <td align="right" style="width: 170px; height: 25px; background-color: #D6E2F3">
            </td>
            <td style="padding-left: 5px; height: 25px; background-color: #ffffff">
                <asp:Button ID="Button1" runat="server" Text="通过审核" 
                    OnClientClick="return shenhe();" OnClick="Button1_Click"  Width="78px" 
                    BackColor="#00CC00" ForeColor="White" Height="38px" />
                <asp:Button ID="Button2" runat="server" Text="驳回到发文人" OnClick="Button2_Click" 
                    Width="98px" BackColor="Red" ForeColor="White" Height="38px" />
                <asp:Button ID="Button3" runat="server" Text="指定其他节点" OnClick="Button3_Click" 
                    Width="144px" BackColor="#FF9933" ForeColor="White" Height="38px" />
                <asp:Button ID="Button4" runat="server" Text="不通过" OnClick="Button4_Click" 
                    Width="90px" BackColor="#FF6600" ForeColor="White" Height="38px" />
                <asp:Button ID="Button5" runat="server" Text="追加审批人" 
                    OnClientClick="SelectUser();" OnClick="Button5_Click" Width="92px" 
                    BackColor="#FF6600" ForeColor="White" Height="38px"/>
               
            </td>
        </tr>
        <tr>
            <td align="right" colspan="2" style="height: 25px; background-color: #D6E2F3; text-align: center">
                <strong>审批记录</strong></td>
        </tr>
        <tr>
            <td align="right" style="width: 170px; height: 25px; background-color: #D6E2F3">
                审批记录：</td>
            <td style="padding-left: 5px; height: 25px; background-color: #ffffff">
                <asp:Label ID="Label5" runat="server"></asp:Label></td>
        </tr>
        </table></div>
        
        <script>
		//批量设置字段的可写与保密属性
		<%=PiLiangSet %>
		
		</script>
        
                 <SCRIPT>
function Load_Do()
{
setTimeout("Load_Do()",0);
var content = document.getElementById("Label3").innerHTML
document.getElementById("TextBox3").value=content;
}
</SCRIPT> 

    </form>
</body>
</html>