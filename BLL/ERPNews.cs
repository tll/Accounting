﻿using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using ZWL.DBUtility;//请先添加引用
namespace ZWL.BLL
{
    public class ERPNews
    {
        public ERPNews()
        { }
        #region Model
        private int _id;
        private DateTime _sdate;
        private string _stype;
        private string _title;
        private string _sfrom;
        private string _scontent;
        private string _spic;
        private string _stop;
        private string _author;
        private string _sroles;
        /// <summary>
        /// 
        /// </summary>
        public int id
        {
            set { _id = value; }
            get { return _id; }
        }
        /// <summary>
        /// 
        /// </summary>
        public DateTime sDate
        {
            set { _sdate = value; }
            get { return _sdate; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string sType
        {
            set { _stype = value; }
            get { return _stype; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string Title
        {
            set { _title = value; }
            get { return _title; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string sFrom
        {
            set { _sfrom = value; }
            get { return _sfrom; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string sContent
        {
            set { _scontent = value; }
            get { return _scontent; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string sPic
        {
            set { _spic = value; }
            get { return _spic; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string sTop
        {
            set { _stop = value; }
            get { return _stop; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string author
        {
            set { _author = value; }
            get { return _author; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string sRoles
        {
            set { _sroles = value; }
            get { return _sroles; }
        }
        #endregion Model


        /// <summary>
        /// 是否存在该记录
        /// </summary>
        public bool Exists(int id)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select count(1) from ERPNews");
            strSql.Append(" where id=@id");
            SqlParameter[] parameters = {
					new SqlParameter("@id", SqlDbType.Int,4)
			};
            parameters[0].Value = id;

            return DbHelperSQL.Exists(strSql.ToString(), parameters);
        }
        /// <summary>
        /// 增加一条数据
        /// </summary>
        public int Add(ERPNews model)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("insert into ERPNews(");
            strSql.Append("sDate,sType,Title,sFrom,sContent,sPic,sTop,author,sRoles)");
            strSql.Append(" values (");
            strSql.Append("@sDate,@sType,@Title,@sFrom,@sContent,@sPic,@sTop,@author,@sRoles)");
            strSql.Append(";select @@IDENTITY");
            SqlParameter[] parameters = {
					new SqlParameter("@sDate", SqlDbType.DateTime,20),
					new SqlParameter("@sType", SqlDbType.NVarChar,50),
					new SqlParameter("@Title", SqlDbType.NVarChar,50),
					new SqlParameter("@sFrom", SqlDbType.NChar,20),
					new SqlParameter("@sContent", SqlDbType.Text),
					new SqlParameter("@sPic", SqlDbType.NVarChar,100),
					new SqlParameter("@sTop", SqlDbType.NChar,10),
					new SqlParameter("@author", SqlDbType.NChar,20),
					new SqlParameter("@sRoles", SqlDbType.Int)};
            parameters[0].Value = model.sDate;
            parameters[1].Value = model.sType;
            parameters[2].Value = model.Title;
            parameters[3].Value = model.sFrom;
            parameters[4].Value = model.sContent;
            parameters[5].Value = model.sPic;
            parameters[6].Value = model.sTop;
            parameters[7].Value = model.author;
            parameters[8].Value = model.sRoles;

            object obj = DbHelperSQL.GetSingle(strSql.ToString(), parameters);
            if (obj == null)
            {
                return 0;
            }
            else
            {
                return Convert.ToInt32(obj);
            }
        }
        /// <summary>
        /// 更新一条数据
        /// </summary>
        public bool Update(ERPNews model)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("update ERPNews set ");
            strSql.Append("sDate=@sDate,");
            strSql.Append("sType=@sType,");
            strSql.Append("Title=@Title,");
            strSql.Append("sFrom=@sFrom,");
            strSql.Append("sContent=@sContent,");
            strSql.Append("sPic=@sPic,");
            strSql.Append("sTop=@sTop,");
            strSql.Append("author=@author,");
            strSql.Append("sRoles=@sRoles");
            strSql.Append(" where id=@id");
            SqlParameter[] parameters = {
					new SqlParameter("@sDate", SqlDbType.DateTime,12),
					new SqlParameter("@sType", SqlDbType.NVarChar,50),
					new SqlParameter("@Title", SqlDbType.NVarChar,50),
					new SqlParameter("@sFrom", SqlDbType.NChar,20),
					new SqlParameter("@sContent", SqlDbType.Text),
					new SqlParameter("@sPic", SqlDbType.NVarChar,100),
					new SqlParameter("@sTop", SqlDbType.NChar,10),
					new SqlParameter("@author", SqlDbType.NChar,20),
                    new SqlParameter("@sRoles", SqlDbType.Int),
					new SqlParameter("@id", SqlDbType.Int,4)};
            parameters[0].Value = model.sDate;
            parameters[1].Value = model.sType;
            parameters[2].Value = model.Title;
            parameters[3].Value = model.sFrom;
            parameters[4].Value = model.sContent;
            parameters[5].Value = model.sPic;
            parameters[6].Value = model.sTop;
            parameters[7].Value = model.author;
            parameters[8].Value = model.sRoles;
            parameters[9].Value = model.id;
            int rows = DbHelperSQL.ExecuteSql(strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        /// <summary>
        /// 删除一条数据
        /// </summary>
        public bool Delete(int id)
        {

            StringBuilder strSql = new StringBuilder();
            strSql.Append("delete from ERPNews ");
            strSql.Append(" where id=@id");
            SqlParameter[] parameters = {
					new SqlParameter("@id", SqlDbType.Int,4)
			};
            parameters[0].Value = id;

            int rows = DbHelperSQL.ExecuteSql(strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        /// <summary>
        /// 批量删除数据
        /// </summary>
        public bool DeleteList(string idlist)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("delete from ERPNews ");
            strSql.Append(" where id in (" + idlist + ")  ");
            int rows = DbHelperSQL.ExecuteSql(strSql.ToString());
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        /// <summary>
        /// 获得数据列表
        /// </summary>
        public DataSet GetList(string strWhere)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select id,sDate,sType,Title,sFrom,sContent,sPic,sTop,author,sRoles ");
            strSql.Append(" FROM ERPNews ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            return DbHelperSQL.Query(strSql.ToString());
        }
        /// <summary>
        /// 获得数据列表-包含对应权限范围
        /// </summary>
        public DataSet GetList2(string strWhere)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select id,sDate,sType,Title,sFrom,sContent,sPic,sTop,author,case sRoles when 1 then '全体教工' when 2 then '中层以上' when 3 then '高层以上' end sRoles ");
            strSql.Append(" FROM ERPNews ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            return DbHelperSQL.Query(strSql.ToString());
        }
        /// <summary>
        /// 获得前几行数据
        /// </summary>
        public DataSet GetList(int Top, string strWhere, string filedOrder)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select ");
            if (Top > 0)
            {
                strSql.Append(" top " + Top.ToString());
            }
            strSql.Append(" id,sDate,sType,Title,sFrom,sContent,sPic,sTop,author,sRoles ");
            strSql.Append(" FROM ERPNews ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            strSql.Append(" order by " + filedOrder);
            return DbHelperSQL.Query(strSql.ToString());
        }
        /// <summary>
        /// 获取记录总数
        /// </summary>
        public int GetRecordCount(string strWhere)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select count(1) FROM ERPNews ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            object obj = DbHelperSQL.GetSingle(strSql.ToString());
            if (obj == null)
            {
                return 0;
            }
            else
            {
                return Convert.ToInt32(obj);
            }
        }
        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("SELECT * FROM ( ");
            strSql.Append(" SELECT ROW_NUMBER() OVER (");
            if (!string.IsNullOrEmpty(orderby.Trim()))
            {
                strSql.Append("order by T." + orderby);
            }
            else
            {
                strSql.Append("order by T.id desc");
            }
            strSql.Append(")AS Row, T.*  from ERPNews T ");
            if (!string.IsNullOrEmpty(strWhere.Trim()))
            {
                strSql.Append(" WHERE " + strWhere);
            }
            strSql.Append(" ) TT");
            strSql.AppendFormat(" WHERE TT.Row between {0} and {1}", startIndex, endIndex);
            return DbHelperSQL.Query(strSql.ToString());
        }
    }
}